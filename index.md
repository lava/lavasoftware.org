---
title: Home
subtitle: Linaro Automated Validation Architecture
layout: page
callouts: home_callouts
hide_footer: true
---

# What is LAVA

LAVA stands for Linaro Automated Validation Architecture.

LAVA is a continuous integration system for deploying operating systems onto
physical and virtual hardware for running tests.

Tests can be:

* simple boot testing
* bootloader testing
* system level testing
* ...

Results and logs are tracked over time and can be exported for further
analysis.
